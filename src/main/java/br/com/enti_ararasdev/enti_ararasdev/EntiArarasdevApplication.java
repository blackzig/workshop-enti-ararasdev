package br.com.enti_ararasdev.enti_ararasdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntiArarasdevApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntiArarasdevApplication.class, args);
	}
}
